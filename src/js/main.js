/*
ON LOAD PAGE FUNCTION
*/

jQuery(window).on('load', function () {

    // Remove preloader after page was loaded
    $('body').removeClass('is-load');

});

/*
INITIALIZATION FUNCTIONS
*/

jQuery(document).ready(function ($) {

    panelSearch(); // Show / Hide search bar
    initSliders(); // Initialization sliders

});

/*
ON SCROLL PAGE FUNCTIONS
*/

jQuery(window).on('scroll', function () {



});

// Show / Hide the search bar
function panelSearch() {

    let buttonTogglePanelSearch = $('.js-toggle-search'),
        formSearch = $('#js-form-search'),
        panelSearch = $('.js-box-search'),
        inputSearch = $('.js-search-input'),
        headerToolbar = $('.js-header-toolbar'),
        headerToolbarNav = $('.js-toolbar-nav');

    if ( headerToolbar.length ) {

        buttonTogglePanelSearch.on('click', function () {

            if ( panelSearch.hasClass('is-active') ) {

                formSearch.submit();

            } else {

                headerToolbarNav.addClass('is-hide');
                panelSearch.addClass('is-active');
                inputSearch.focus();

                return false;

            }

        });

    }

    inputSearch.on('keyup', function (e) {

        // If button 'Esc' was pressed on keyboard
        if ( e.keyCode === 27 ) {

            headerToolbarNav.removeClass('is-hide');
            panelSearch.removeClass('is-active');
            $(this).val('');

        }

        // If button 'Enter' was pressed on keyboard
        if ( e.keyCode === 13 ) {

            formSearch.submit();

        }

    });

    // Hide the search bar if u click on the empty space of the document
    $(document).on('click', function (e) {

        if ( !$(e.target).closest(headerToolbar).length ) {

            headerToolbarNav.removeClass('is-hide');
            panelSearch.removeClass('is-active');
            inputSearch.val('');

        }

        e.stopPropagation();

    });

}

// Initialization sliders
function initSliders() {

    let sliderMain = $('.js-main-slider');

    if ( sliderMain.length ) {

        sliderMain.slick({
            infinite: true,
            //autoplay: true,
            autoplaySpeed: 3000
        })

    }

}